
var fs = require('fs');
/*
 * GET home page.
 */

var dataDir = './data';



var getBenchmarks = function() {
   var dirs = fs.readdirSync(dataDir).sort();
   var tmp = new Array(dirs.length); 
   for(var i in dirs ) {
      tmp[i] = {name: dirs[i]};
   };
   return tmp;
};

var importNodes = function(nodesDir, traces) {
   var nodeFiles = fs.readdirSync(nodesDir).sort();
   var nodes = new Array(nodeFiles.length);
   for(var i in nodeFiles) {
      var filePath = nodesDir+'/'+nodeFiles[i];
      // var pos = traces.push(fs.readFileSync(filePath).toJSON());
      var pos = traces.push(fs.readFileSync(filePath).toString().split("\n"));
      
      nodes[i] = {name: nodeFiles[i], traceIndex: pos-1};
   }
   return nodes;
}

var importTraces = function(bm, traces) {
   
   var tracesDir = dataDir+'/'+bm;
   var traceFiles = fs.readdirSync(tracesDir).sort()
   var tracesMeta = new Array(traceFiles.length);
   for (var i in traceFiles) {
      tracesMeta[i] = {  name: traceFiles[i], 
                     nodes: importNodes(tracesDir+'/'+traceFiles[i], traces)};
   };
   console.log(bm);
   return tracesMeta;
};

exports.index = function(req, res){
   
   var benchmarks =  getBenchmarks();
   var traces= [];
   for (var i in benchmarks) {
      benchmarks[i].tracesMeta = importTraces(benchmarks[i].name, traces);
   }
   
   
   // res.writeHead(200, { 'Content-Type': 'application/json' });
   // res.write(JSON.stringify({benchmarks: benchmarks}));
   // res.end();
  
  res.render('index', { title: 'Express', bm: benchmarks, traces: traces });
};